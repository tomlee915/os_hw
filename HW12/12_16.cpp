#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <algorithm>

int max(int request[])
{
	int max = 0;
	for(int i = 0; i < 1000; i++)
		if(max < request[i])
			max = request[i];
	return max;
}

int min(int request[])
{
	int min = 4999;
	for(int i = 0; i < 1000; i++)
		if(min > request[i])
			min = request[i];
	return min;
}

int maxLowerThanStart(int start, int request[])
{
	int max = min(request);
	for(int i = 0; i < 1000; i++)
	{
		if(request[i] > max && request[i] <= start)
			max = request[i];
	}
	return max;
}

int minHigherThanStart(int start, int request[])
{
	int min = max(request);
	for(int i = 0; i < 1000; i++)
	{
		if(request[i] < min && request[i] >= start)
			min = request[i];
	}
	return min;
}

int fcfs(int start, int request[])
{
	int count = abs(start - request[0]);
	for (int i = 0; i < 999; i++)
		count += abs(request[i] - request[i + 1]);
	return count;
}

int nearest(int now, int request[])
{
	int min = 4999, index;

	for (int i = 0; i < 1000; i++)
		if(min > abs(request[now] - request[i]) && now != i)
		{
			min = abs(request[now] - request[i]);
			index = i;
		}
	return index;
}

int sstf(int start, int request[])
{
	int temp[1001];
	for (int i = 0; i < 1000; i++)
		temp[i] = request[i];
	temp[1000] = start;

	int now = nearest(1000, temp);
	int count = abs(start - temp[now]);

	for (int i = 0; i < 999; i++)
	{
		int next = nearest(now, temp);
		count += abs(temp[now] - temp[next]);
		temp[now] = 10001;
		now = next;
	}

	return count;
}

int scan(int start, int request[])
{
	int forward = 9998 - start - min(request);
	int backward = start + max(request);
	return std::min(forward, backward);
}

int c_scan(int start, int request[])
{
	int forward = 9998 - (start - maxLowerThanStart(start, request));
	int backward = 9998 - (minHigherThanStart(start, request) - start);
	return std::min(forward, backward);
}

int look(int start, int request[])
{
	int forward = 2 * max(request) - start - min(request);
	int backward = start + max(request) - 2 * min(request);
	return std::min(forward, backward);
}

int c_look(int start, int request[])
{
	int forward = 2 * (max(request) - min(request)) - (start - maxLowerThanStart(start, request));
	int backward = 2 * (max(request) - min(request)) - (minHigherThanStart(start, request) - start);
	return std::min(forward, backward);
}

int main(int argc, char *argv[])
{
	int start = atoi(argv[1]);
	unsigned seed = (unsigned)time(NULL);
	srand(seed);
	int request[1000];
	for(int i = 0; i < 1000; i++)
		request[i] = rand() % 5000;
	std::cout << "FCFS : " << fcfs(start, request) << std::endl; 
	std::cout << "SSTF : " << sstf(start, request) << std::endl; 
	std::cout << "SCAN : " << scan(start, request) << std::endl; 
	std::cout << "C-SCAN : " << c_scan(start, request) << std::endl; 
	std::cout << "LOOK : " << look(start, request) << std::endl; 
	std::cout << "C-LOOK : " << c_look(start, request) << std::endl;
}

