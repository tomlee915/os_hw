import java.net.*;
import java.io.*;
import java.util.Random;

public class DateServer_3_16
{
	public static void main(String[] args)  {

		String[] quotes = {
			"When a human was killed, he would died.",
			"In Aferica, every sixty seconds, there is one minute to pass.",
			"When Taiwanese people are sleeping, most American are working hard.",
			"When your left cheek get hit, your left cheek will be hurt."
		};
		try {
			ServerSocket sock = new ServerSocket(6017);

			// now listen for connections
			while (true) {
				Socket client = sock.accept();
				// we have a connection
				
				PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
				Random r = new Random();

				pout.println(quotes[r.nextInt(quotes.length)]);

				// close the socket and resume listening for more connections
				client.close();
			}
		}
		catch (IOException ioe) {
				System.err.println(ioe);
		}
	}
}
