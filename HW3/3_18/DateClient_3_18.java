import java.net.*;
import java.io.*;
import java.util.Scanner;

public class DateClient_3_18
{
	public static void main(String[] args)  {
		String line;
		Scanner input = new Scanner(System.in);
		try {
			// this could be changed to an IP name or address other than the localhost
			Socket sock = new Socket("127.0.0.1",6017);
			InputStream in = sock.getInputStream();
			BufferedReader bin = new BufferedReader(new InputStreamReader(in));
			line = input.nextLine();

			PrintWriter pout = new PrintWriter(sock.getOutputStream(), true);
			pout.println(line);

			while( (line = bin.readLine()) != null)
				System.out.println(line);
				
			sock.close();
		}
		catch (IOException ioe) {
				System.err.println(ioe);
		}
	}
}
