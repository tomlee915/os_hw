import java.net.*;
import java.io.*;

public class DateServer_3_18
{
	public static void main(String[] args)  {
		String echo;

		try {
			ServerSocket sock = new ServerSocket(6017);
			while (true) {
				Socket client = sock.accept();
				InputStream in = client.getInputStream();
				BufferedReader bin = new BufferedReader(new InputStreamReader(in));

				if( (echo = bin.readLine()) != null)
				{
					System.out.println(echo);
					echo = "Echo : " + echo;
				}

				PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
				pout.println(echo);

				// close the socket and resume listening for more connections
				client.close();
			
			}
		}
		catch (IOException ioe) {
				System.err.println(ioe);
		}
	}
}
