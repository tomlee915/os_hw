#include <stdio.h>

#include <stdlib.h>

#include <pthread.h>

#include <unistd.h>

#include "buffer.h"

void print_instr() {

printf("Please provide\n\

\t1. How long to sleep before terminating.\n\

\t2. The number of producer threads.\n\

\t3. The number of consumer threads.\n");

}

int main(int argc, char *argv[]) {

if(argc <= 3) { // To make sure the proper input is there, prompt the

user if not

print_instr();

exit(0);

}

int sleep_duration = atoi(argv[1]);

int num_producers = atoi(argv[2]);

int num_consumers = atoi(argv[3]);

init_buffer(); //print_buffer(buffer); For testing, currently commented out in buffer.h

int i = 0;

for(;i<num_producers;i++) {

pthread_t tid;

pthread_attr_t attr;

pthread_attr_init(&attr);

pthread_create(&tid, &attr, producer, NULL);

}

i = 0;

for(;i<num_consumers;i++) {

pthread_t tid;

pthread_attr_t attr;

pthread_attr_init(&attr);

pthread_create(&tid, &attr, consumer, NULL);

}

sleep(sleep_duration);

//print_buffer(buffer);

return 0;

}