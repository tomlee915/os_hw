#ifndef BUFFER_H

#define BUFFER_H

typedef int buffer_item;

#define BUFFER_SIZE 5

unsigned int junk;

buffer_item buff[BUFFER_SIZE];

int insert_item(buffer_item);

int remove_item(buffer_item *);

void init_buffer(); //void print_buffer(); for testing only

void *producer(void *param);

void *consumer(void *param);

#endif