#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h> 
#include <linux/list.h> 
#include <linux/types.h>
#include <linux/slab.h> 

struct birthday
{
    int day;
    int month;
    int year;
    struct list_head list;
};


LIST_HEAD(birthday_list);

int birthdayList_init(void)
{
    struct birthday *person, *ptr;
    int i = 0;

    printk(KERN_INFO "Create list.\n");


    for(i = 0; i < 5; i++)
	{
        person = kmalloc(sizeof(*person), GFP_KERNEL);
        person->day = 10 + i;
        person->month = 1 + i;
        person->year = 2000 + i;
        INIT_LIST_HEAD(&person->list);
        list_add_tail(&person->list, &birthday_list);
	}

    list_for_each_entry(ptr, &birthday_list, list)
	{
        printk(KERN_INFO "Birthday: Month %d Day %d Year %d\n", ptr->month, ptr->day, ptr->year);
    }
	return 0;
}

void birthdayList_exit(void)
{
    struct birthday *ptr, *next;

    printk(KERN_INFO "Remove list\n");

    list_for_each_entry_safe(ptr, next, &birthday_list, list)
    {

        printk(KERN_INFO "Removing Birthday: Month %d Day %d Year %d\n", ptr->month, ptr->day, ptr->year);

        list_del(&ptr->list);
        kfree(ptr);
    }

    printk(KERN_INFO "Memory free \n");

}

module_init(birthdayList_init);
module_exit(birthdayList_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("SGG");