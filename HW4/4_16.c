#include <pthread.h>
#include <stdio.h>

int avg; /* this data is shared by the thread(s) */
int min;
int max;

void *average(void*); /* the thread */
void *minimum(void*);
void *maximum(void*);

int main(int argc, char *argv[])
{
pthread_t tid[3]; /* the thread identifier */
pthread_attr_t attr[3]; /* set of attributes for the thread */

if (argc < 2) {
	fprintf(stderr,"usage: ./4_16 <integer value> ...\n");
	/*exit(1);*/
	return -1;
}

int i;
int a[argc];

a[0] = argc-1;
for(i = 1; i < argc; i++)
{
	sscanf(argv[i], "%d", &a[i]);
}

pthread_attr_init(&attr[0]);
pthread_attr_init(&attr[1]);
pthread_attr_init(&attr[2]);

pthread_create(&tid[0], &attr[0], average, a);
pthread_create(&tid[1], &attr[1], minimum, a);
pthread_create(&tid[2], &attr[2], maximum, a);

pthread_join(tid[0],NULL);
pthread_join(tid[1],NULL);
pthread_join(tid[2],NULL);

printf("The average value is %d\n",avg);
printf("The minimum value is %d\n",min);
printf("The maximum value is %d\n",max);
}

/**
 * The thread will begin control in this function
 */
void* average(void* a) 
{
	int *b = (int*)a;
	int i;
	avg = 0;

	for (i = 1; i <= *b; i++)
		avg += *(b+i);

	avg = avg / *b;
}

void* minimum(void* a)
{
	int i;
	int *b = (int*)a;
	min = *(b+1);

	for (i = 2; i <= *b; i++)
		if(min > *(b+i))
			min = *(b+i);
}

void* maximum(void* a)
{
	int i;
	int *b = (int*)a;
	max = *(b+1);

	for (i = 2; i <= *b; i++)
		if(max < *(b+i))
			max = *(b+i);
}